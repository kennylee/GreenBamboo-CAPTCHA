#! /bin/bash

version=20180613
repositoryId=thirdparty

mvn deploy:deploy-file -DgroupId=pub.greenbamboo \
  -DartifactId=CAPTCHA \
  -Dversion=1.0-r$version \
  -Dpackaging=jar \
  -Dfile=`pwd`/target/CAPTCHA-1.0-r$version.jar \
  -DrepositoryId=$repositoryId \
  -Durl=http://maven.kennylee.me:8081/nexus/content/repositories/tkparty
