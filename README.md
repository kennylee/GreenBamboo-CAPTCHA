#GreenBamboo-CAPTCHA
青竹验证码系统是互联网开源青竹团队的第一个项目。该项目拥有实现统一图形验证码生成接口的验证码实体类，项目只需调用各种验证码实例即可轻松获得验证的能力。 

更多关于项目介绍，请查看Readme文件。

更多关于青竹开源团队，请查看https://team.oschina.net/team_profile/GreenBamboo 

如果，您使用青竹团队的开源项目，帮助您获得了成功或者收益。请捐助我们，让我们可以走过更加长远的路，好为您以及其他人提供更加多的开源项目和帮助。

帮助他人，也是帮助自己，他人成全了您的成功，您成功了是不是应该回馈帮助过您的组织或者个人呢？只有用心支持有价值的事物，才能让自己的世界更加美好！

请扫描如下二维码：

支付宝请扫描

![输入图片说明](http://git.oschina.net/uploads/images/2016/0626/103546_dc8b9d4c_19694.png "在这里输入图片标题")

微信请扫描

![输入图片说明](http://git.oschina.net/uploads/images/2016/0626/103630_b93c99b0_19694.png "在这里输入图片标题")


DefaultCaptcha

![输入图片说明](http://git.oschina.net/uploads/images/2016/0620/133026_a0ee7aab_19694.png "在这里输入图片标题")


ACaptcha

![输入图片说明](http://git.oschina.net/uploads/images/2016/0620/132509_db0ece28_19694.png "在这里输入图片标题")

BCaptcha

![输入图片说明](http://git.oschina.net/uploads/images/2016/0620/132659_96d63d8f_19694.png "在这里输入图片标题")

DCaptcha

![输入图片说明](http://git.oschina.net/uploads/images/2016/0620/132824_03c19266_19694.png "在这里输入图片标题")

ECaptcha

![输入图片说明](http://git.oschina.net/uploads/images/2016/0620/133110_7bd1f71f_19694.png "在这里输入图片标题")

FCaptcha

![输入图片说明](http://git.oschina.net/uploads/images/2016/0620/133141_5d802e09_19694.png "在这里输入图片标题")


GCaptcha

![输入图片说明](http://git.oschina.net/uploads/images/2016/0620/133211_915deede_19694.png "在这里输入图片标题")


HCaptcha


![输入图片说明](http://git.oschina.net/uploads/images/2016/0622/164647_12870d36_19694.png "在这里输入图片标题")

JCaptcha

![输入图片说明](http://git.oschina.net/uploads/images/2016/0622/201507_7c7dafe6_19694.png "在这里输入图片标题")

KCaptcha

![输入图片说明](http://git.oschina.net/uploads/images/2016/0704/153924_60740927_19694.png "在这里输入图片标题")


Captcha1 纯数字验证码

![输入图片说明](http://git.oschina.net/uploads/images/2016/0728/151012_6ecd6122_19694.png "在这里输入图片标题")

Captcha2 小写字母验证码

![输入图片说明](http://git.oschina.net/uploads/images/2016/0728/150002_f9e8efbb_19694.png "在这里输入图片标题")

Captcha3 大写字母验证码

![输入图片说明](http://git.oschina.net/uploads/images/2016/0728/150045_01aeb8f0_19694.png "在这里输入图片标题")

Captcha4 小写字母+大写字母验证码

![输入图片说明](http://git.oschina.net/uploads/images/2016/0728/150229_6f65ef37_19694.png "在这里输入图片标题")

Captcha5 数字+小写字母验证码

![输入图片说明](http://git.oschina.net/uploads/images/2016/0728/150141_a5038132_19694.png "在这里输入图片标题")

Captcha6 数字+大写字母验证码

![输入图片说明](http://git.oschina.net/uploads/images/2016/0728/150408_05441bee_19694.png "在这里输入图片标题")

Captcha7 数字+小写字母+大写字母验证码

![输入图片说明](http://git.oschina.net/uploads/images/2016/0728/150511_ac133b31_19694.png "在这里输入图片标题")

Captcha8 成语验证码

![输入图片说明](http://git.oschina.net/uploads/images/2016/0728/152334_fc6fd874_19694.png "在这里输入图片标题")

Captcha9 波浪干线扰验证码

![输入图片说明](http://git.oschina.net/uploads/images/2016/0728/155510_24851031_19694.png "在这里输入图片标题")

Captcha10 噪点干扰验证码

![输入图片说明](http://git.oschina.net/uploads/images/2016/0728/161233_c8474e43_19694.png "在这里输入图片标题")

Captcha11 同心圆干扰验证码

![输入图片说明](http://git.oschina.net/uploads/images/2016/0728/172253_ccdf1123_19694.png "在这里输入图片标题")

Captcha12 斜线干扰验证码

![输入图片说明](http://git.oschina.net/uploads/images/2016/0729/092246_5b2be93d_19694.png "在这里输入图片标题")


Captcha13 网格干扰验证码

![输入图片说明](http://git.oschina.net/uploads/images/2016/0729/093504_3df9a873_19694.png "在这里输入图片标题")

Captcha14

![输入图片说明](http://git.oschina.net/uploads/images/2016/0729/103407_1c91277b_19694.png "在这里输入图片标题")

Captcha15

![输入图片说明](http://git.oschina.net/uploads/images/2016/0729/155210_946364b8_19694.png "在这里输入图片标题")

Captcha16



Captcha17



Captcha18



Captcha19



Captcha20



Captcha21



Captcha22






GreenBamboo-CAPTCHA调用样例：

```
//生成 **验证码图像** 和对应 **字符串验证码** 

CaptchaContext cc = new CaptchaContext(ACaptcha.CODEER_NAME);

ImageCode ic = cc.getCaptcha();//获取验证码和字符串


//校验验证码
String codeKey=;//来自用户浏览器图像验证码对应验证码

String clientCode=;//来自浏览器用户输入的验证码

boolean isSame = cc.checkCaptcha(codeKey,clientCode);
```